@extends('layouts.app')

@section('body')
    <div class="container-fluid">

        <!-- Page Heading -->
        <h1 class="h3 mb-2 text-gray-800">Symptoms</h1>

        <!-- DataTales Example -->
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Symptoms and Defficiencies</h6>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                            <tr>
                                <th>Symptom</th>
                                <th>Defficiencies</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($symptoms as $symptom)
                                <tr>
                                    <td>{{ $symptom->name }}</td>
                                    <td>
                                        <ul>
                                            @foreach ($symptom->defficiencies as $d)
                                                <li>{{ $d->name }}</li>
                                            @endforeach
                                        </ul>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    </div>
@endsection
