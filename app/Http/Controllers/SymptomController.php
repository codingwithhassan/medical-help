<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Symptom;

class SymptomController extends Controller
{
    public function index()
    {
        $symptoms = Symptom::all();
        return view('index', compact('symptoms'));
    }
}
