<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use App\Models\Symptom;
use App\Models\Defficiency;

class DefficiencySymptomSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for($i = 0; $i < 10; $i++){
            $symptom = Symptom::factory()->create();
            $defficiencies = Defficiency::factory(3)->create();

            $symptom->defficiencies()->attach($defficiencies);
        }
    }
}
