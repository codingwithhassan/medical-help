<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('defficiency_symptom', function (Blueprint $table) {
            $table->unsignedBigInteger('defficiency_id');
            $table->unsignedBigInteger('symptom_id');

            $table->foreign('defficiency_id')->references('id')->on('defficiencies')->onDelete('CASCADE');
            $table->foreign('symptom_id')->references('id')->on('symptoms')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('defficiency_symptom');
    }
};
