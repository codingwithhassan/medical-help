<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use App\Models\Symptom;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Symptom>
 */
class SymptomFactory extends Factory
{
    /**
    * The name of the factory's corresponding model.
    *
    * @var string
    */
   protected $model = Symptom::class;

    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'name' => $this->faker->text(20),
        ];
    }
}
